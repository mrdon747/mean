name             'MEAN'
maintainer       'YOUR_COMPANY_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures MEAN'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'docker', '~> 2.5.8'
depends 'apt', '~> 2.9.2'
depends 'nodejs', '~> 2.4.4'
