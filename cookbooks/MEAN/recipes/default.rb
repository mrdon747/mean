#
# Cookbook Name:: MEAN
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# install docker from it's official repo
package 'docker-engine' do
	action :nothing
end

# Add apt repo for docker official.
apt_repository 'Dockerproject' do
	action :add
	uri 'https://apt.dockerproject.org/repo'
	components ['main']
	distribution 'ubuntu-trusty'
	keyserver 'hkp://p80.pool.sks-keyservers.net:80'
	key '58118E89F3A912897C070ADBF76221572C52609D'
	notifies :install, 'package[docker-engine]', :immediately
end

# start docker service
docker_service 'default' do
	action [:create, :start]
end

# make sure the docker group exists and the user is part of it.
group 'docker' do
	action :create
	members ['vagrant']
end

# pull the containers we need.
docker_image 'mongo' do
	tag '2.6'
end

include_recipe "nodejs"

# need to install sass
gem_package 'sass' do
	action :install
end

# install global npm packages
['bower', 'grunt-cli'].each do |packageName|
  nodejs_npm packageName do
    action :install
  end
end
