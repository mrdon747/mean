# override nodejs packagenames
override['nodejs']['packages'] = ['nodejs', 'npm']
override['nodejs']['version'] = '4.4.3'
override['nodejs']['binary']['checksum']["linux_x64"] = "28ff2b23a837526ecfea66b0db42d43ec84368949998f2cb26dd742e8988ec1f"
override['nodejs']['install_method'] = 'binary'
default['MEAN']['application_name'] = 'AppName'
